package com.example.LenDenRunner.LenDenRunner.controller;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FrontendController {

    @GetMapping("/")
    public String home(Model model) {
        return  "index";
    }

    @PostMapping("/")
    public String submitForm(Model model){
        return "razorpay-payment";
    }
}
