package com.example.LenDenRunner.LenDenRunner.controller;

import com.coditas.lenThen.razorpay.dto.request.CapturePaymentRequest;
import com.coditas.lenThen.razorpay.dto.request.CreateOrderRequest;
import com.coditas.lenThen.razorpay.dto.request.PaymentLinkRequest;
import com.coditas.lenThen.razorpay.dto.request.RazorpayRefundRequest;
import com.coditas.lenThen.razorpay.dto.response.OrderResponse;
import com.coditas.lenThen.razorpay.dto.response.OrderVerificationResponse;
import com.coditas.lenThen.razorpay.dto.response.PaymentResponse;
import com.coditas.lenThen.razorpay.dto.response.RazorpayRefundResponse;
import com.coditas.lenThen.razorpay.service.OrderService;
import com.coditas.lenThen.razorpay.service.PaymentService;
import com.coditas.lenThen.razorpay.service.RazorpayRefundService;
import com.razorpay.RazorpayException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/razorPay")
public class RazorPayController {

    @Autowired(required = false)
    private OrderService orderService;

    @Autowired(required = false)
    private PaymentService paymentService;

    @Autowired(required = false)
    private RazorpayRefundService razorpayRefundService;

    @PostMapping("/order/create")
    public ResponseEntity<OrderResponse> createOrder(@RequestBody CreateOrderRequest createOrderRequest) throws RazorpayException {
        OrderResponse orderResponse = orderService.createOrder(createOrderRequest);
        return new ResponseEntity<>(orderResponse, HttpStatus.CREATED);
    }

    @GetMapping("/order/getOrderById/{orderId}")
    public ResponseEntity<OrderResponse> getOrderById(@PathVariable String orderId){
        OrderResponse orderResponse = orderService.getOrderById(orderId);
        return new ResponseEntity<>(orderResponse, HttpStatus.OK);
    }

    @GetMapping("/order/getAllOrder")
    public ResponseEntity<List<OrderResponse>> getAllOrder() throws RazorpayException {
        List<OrderResponse> orderResponse = orderService.getAllOrders();
        return new ResponseEntity<>(orderResponse, HttpStatus.OK);
    }

    @PostMapping("/refund/create")
    public ResponseEntity<RazorpayRefundResponse> createRefund(@RequestBody RazorpayRefundRequest refundRequest){
        RazorpayRefundResponse razorpayRefundResponse = razorpayRefundService.createRefund(refundRequest);
        return new ResponseEntity<>(razorpayRefundResponse, HttpStatus.CREATED);
    }

    @GetMapping("/refund/getAllRefunds")
    public ResponseEntity<List<RazorpayRefundResponse>> getAllRefunds() throws RazorpayException {
        List<RazorpayRefundResponse> razorpayRefundResponses = razorpayRefundService.getAllRefunds();
        return new ResponseEntity<>(razorpayRefundResponses, HttpStatus.OK);
    }

    @PostMapping("/payment/capturePayment")
    public ResponseEntity<PaymentResponse> capturePayment(@RequestBody CapturePaymentRequest capturePaymentRequest){
        PaymentResponse paymentResponse = paymentService.capturePayment(capturePaymentRequest);
        return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
    }

    @RequestMapping(path = "/payment/verifyPaymentSignature",method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<OrderVerificationResponse> verifyPaymentSignature(@RequestParam("razorpay_payment_id") String razorpayPaymentId,
                                                                            @RequestParam("razorpay_order_id") String razorpayOrderId,
                                                                            @RequestParam("razorpay_signature") String razorSignature) throws NoSuchAlgorithmException, InvalidKeyException {
        OrderVerificationResponse orderVerificationResponse = paymentService.verifyPaymentSignature(OrderVerificationResponse.builder().razorpayPaymentId(razorpayPaymentId).razorpayOrderId(razorpayOrderId).razorSignature(razorSignature).build());
        return new ResponseEntity<>(orderVerificationResponse, HttpStatus.OK);
    }

    @GetMapping("/payment/getPaymentById/{paymentId}")
    public ResponseEntity<PaymentResponse> getPaymentById(@PathVariable String paymentId) throws RazorpayException {
        PaymentResponse paymentResponse = paymentService.getPaymentById(paymentId);
        return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
    }

    @GetMapping("/payment/getAllPayments")
    public ResponseEntity<List<PaymentResponse>> getAllPayments() throws RazorpayException {
        List<PaymentResponse> paymentResponse = paymentService.getAllPayments();
        return new ResponseEntity<>(paymentResponse, HttpStatus.OK);
    }

    @PostMapping("/payment/createPaymentLink")
    public ResponseEntity<String> createPaymentLink(@RequestBody PaymentLinkRequest paymentLinkRequest){
        String paymentLink = paymentService.createPaymentLink(paymentLinkRequest);
        return new ResponseEntity<>(paymentLink, HttpStatus.OK);
    }

    @PostMapping("/callback-url")
    public String callbackUrl(@RequestBody String request) {
        return "request";
    }
}
