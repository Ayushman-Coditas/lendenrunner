package com.example.LenDenRunner.LenDenRunner.controller;

import com.coditas.lenThen.phonepe.dto.request.InitiatePaymentRequest;
import com.coditas.lenThen.phonepe.dto.response.InitiatePaymentResponse;
import com.coditas.lenThen.phonepe.service.PhonePeService;
import com.phonepe.sdk.pg.common.http.PhonePeResponse;
import com.phonepe.sdk.pg.payments.v1.models.response.PgPayResponse;
import com.phonepe.sdk.pg.payments.v1.models.response.PgRefundResponse;
import com.phonepe.sdk.pg.payments.v1.models.response.PgTransactionStatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/api/phonepe")
public class PhonePeController {
    public PhonePeController() {
    }

    @Autowired(required = false)
    private PhonePeService phonePeService;

    @PostMapping("/initiate-payment")
    public ResponseEntity<InitiatePaymentResponse> initiatePayment(@RequestBody InitiatePaymentRequest initiatePaymentRequest){
        String iniitiateLink = phonePeService.initiatePayment(initiatePaymentRequest);
        InitiatePaymentResponse initiatePaymentResponse = new InitiatePaymentResponse();
        initiatePaymentResponse.setRedirectUrl(iniitiateLink);
        return ResponseEntity.ok(initiatePaymentResponse);
    }

    @PostMapping("/redirect-page")
    public PgPayResponse redirect(PgPayResponse request){
        return request;
    }

    @PostMapping("/callback-url")
    public String callbackUrl(@RequestBody String request){
        return "request";
    }

    @GetMapping("/payment-status")
    public ResponseEntity<PhonePeResponse<PgTransactionStatusResponse>> checkStatus(@RequestParam String merchantTransactionId){
        PhonePeResponse<PgTransactionStatusResponse> response = phonePeService.checkStatus(merchantTransactionId);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/refund")
    public ResponseEntity<PhonePeResponse<PgRefundResponse>> refund(@RequestParam String originalTransactionId, @RequestParam Long amount){
        PhonePeResponse<PgRefundResponse> response = phonePeService.refund(originalTransactionId, amount);
        return ResponseEntity.ok(response);
    }
}
