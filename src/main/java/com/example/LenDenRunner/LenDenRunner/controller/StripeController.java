package com.example.LenDenRunner.LenDenRunner.controller;

import com.coditas.lenThen.common.dto.request.RefundRequest;
import com.coditas.lenThen.common.exceptions.StripeGenericException;
import com.coditas.lenThen.stripe.dto.PaymentLinkDto;
import com.coditas.lenThen.stripe.dto.request.CustomerRequest;
import com.coditas.lenThen.stripe.dto.request.CustomerUpdateRequest;
import com.coditas.lenThen.stripe.dto.request.PaymentIntentRequest;
import com.coditas.lenThen.stripe.dto.response.CustomerResponse;
import com.coditas.lenThen.stripe.dto.response.PaymentIntentResponse;
import com.coditas.lenThen.stripe.dto.response.StripeRefundResponse;
import com.coditas.lenThen.stripe.service.CustomerService;
import com.coditas.lenThen.stripe.service.PaymentIntentService;
import com.coditas.lenThen.stripe.service.StripeRefundService;
import com.stripe.exception.StripeException;

import com.stripe.service.RefundService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/stripe")
public class StripeController {

    public StripeController() {
    }

    @Autowired(required = false)
    private CustomerService customerService;

    @Autowired(required = false)
    private RefundService refundService;

    @Autowired(required = false)
    private PaymentIntentService paymentIntentService;

    @Autowired(required = false)
    private StripeRefundService stripeRefundService;


    @PostMapping("/customer/create")
    public ResponseEntity<CustomerResponse> createCustomer(@RequestBody CustomerRequest customerRequest) {
        CustomerResponse customer = customerService.createCustomer(customerRequest);
        return new ResponseEntity<>(customer, HttpStatus.CREATED);
    }

    @GetMapping("/customer/get/{customerId}")
    public ResponseEntity<CustomerResponse> getCustomer(@PathVariable String customerId) {
        CustomerResponse customer = customerService.getCustomer(customerId);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @PutMapping("/customer/update")
    public ResponseEntity<CustomerResponse> updateCustomer(@RequestBody CustomerUpdateRequest customerRequest) {
        CustomerResponse customer = customerService.updateCustomer(customerRequest);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @DeleteMapping("/customer/delete/{customerId}")
    public ResponseEntity<Boolean> deleteCustomer(@PathVariable String customerId) {
        customerService.deleteCustomer(customerId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/paymentIntent/create")
    public ResponseEntity<PaymentIntentResponse> createPaymentIntent(@RequestBody PaymentIntentRequest paymentIntentRequest) {
        PaymentIntentResponse paymentIntentResponse = paymentIntentService.createPaymentIntent(paymentIntentRequest);
        return new ResponseEntity<>(paymentIntentResponse, HttpStatus.OK);
    }

    @GetMapping("/paymentIntent/get/{paymentIntentId}")
    public ResponseEntity<PaymentIntentResponse> getPaymentIntent(@PathVariable String paymentIntentId){
        PaymentIntentResponse paymentIntentResponse = paymentIntentService.getPaymentIntent(paymentIntentId);
        return new ResponseEntity<>(paymentIntentResponse, HttpStatus.OK);
    }

    @GetMapping("/paymentIntent/getPaymentStatus/{paymentIntentId}")
    public ResponseEntity<String> getPaymentStatus(@PathVariable String paymentIntentId){
        String paymentStatus = paymentIntentService.getPaymentStatus(paymentIntentId);
        return new ResponseEntity<>(paymentStatus, HttpStatus.OK);
    }

    @GetMapping("/paymentIntent/getPaymentMethods")
    public ResponseEntity<List<String>> getPaymentMethods(){
        List<String> paymentStatus = paymentIntentService.getPaymentMethods();
        return new ResponseEntity<>(paymentStatus, HttpStatus.OK);
    }

    @PostMapping("/paymentIntent/createPaymentLink")
    public ResponseEntity<String> createPaymentLink(@RequestBody PaymentLinkDto paymentLinkDto){
        String paymentLink = paymentIntentService.createPaymentLink(paymentLinkDto);
        return new ResponseEntity<>(paymentLink, HttpStatus.OK);
    }

    @GetMapping("/paymentIntent/getTransactionsLastNHours/{hours}")
    public ResponseEntity<List<PaymentIntentResponse>> getTransactionsLastNHours(@PathVariable int hours){
        List<PaymentIntentResponse> allTransactions = paymentIntentService.getTransactionsLastNHours(hours);
        return new ResponseEntity<>(allTransactions, HttpStatus.OK);
    }

    @GetMapping("/paymentIntent/getTransactionsForCustomer/{customerId}")
    public ResponseEntity<List<PaymentIntentResponse>> getTransactionsForCustomer(@PathVariable String customerId){
        List<PaymentIntentResponse> allTransactionsForCustomer = paymentIntentService.getTransactionsForCustomer(customerId);
        return new ResponseEntity<>(allTransactionsForCustomer, HttpStatus.OK);
    }

    @PostMapping("/refund/partialRefund")
    public ResponseEntity<StripeRefundResponse> processPartialRefund(@RequestBody RefundRequest refundRequest){
        StripeRefundResponse stripeRefundResponse = stripeRefundService.processPartialRefund(refundRequest);
        return new ResponseEntity<>(stripeRefundResponse, HttpStatus.OK);
    }

    @PostMapping("/refund/refund/{paymentIntentId}")
    public ResponseEntity<StripeRefundResponse> processRefund(@PathVariable String paymentIntentId){
        StripeRefundResponse stripeRefundResponse = stripeRefundService.processRefund(paymentIntentId);
        return new ResponseEntity<>(stripeRefundResponse, HttpStatus.OK);
    }

    @GetMapping("/refund/getAllRefunds/{paymentIntentId}")
    public ResponseEntity<List<StripeRefundResponse>> getAllRefundsForPayment(@PathVariable String paymentIntentId){
        List<StripeRefundResponse> stripeRefundResponse = stripeRefundService.getAllRefundsForPayment(paymentIntentId);
        return new ResponseEntity<>(stripeRefundResponse, HttpStatus.OK);
    }

    @GetMapping("/success-url")
    public String successUrl(@RequestBody String request){
        return "request";
    }

    @PostMapping("/cancel-url")
    public String cancelUrl(@RequestBody String request){
        return "request";
    }

    @GetMapping("/")
    public String home(Model model) {
        return "index";
    }

    @GetMapping("payment/success")
    public String get(HttpServletRequest httpServletRequest){
        return "payment";
    }
}
